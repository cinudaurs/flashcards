package com.google.developer.flashcards;

import android.content.ContentValues;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TextInputEditText;
import android.support.v4.app.Fragment;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.developer.flashcards.data.DatabaseContract;

/**
 * Created by srini on 8/20/16.
 */
public class AddCardFragment extends Fragment
        implements LoaderManager.LoaderCallbacks<Cursor> {

    // constant used to identify the Loader
    private static final int FLASHCARD_LOADER = 0;

    private CardsFragment.CardFragmentListener listener; // MainActivity
    private Uri flashCardUri; // Uri of selected contact
    private boolean addingNewCard = true; // adding (true) or editing

    // EditTexts for contact information
    private TextInputEditText questionTextInputLayout;
    private TextInputEditText answerTextInputLayout;


    private CoordinatorLayout coordinatorLayout; // used with SnackBars

    // called when Fragment's view needs to be created
    @Override
    public View onCreateView(
            LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        setHasOptionsMenu(true); // fragment has menu items to display

        // inflate GUI and get references to EditTexts
        View view =
                inflater.inflate(R.layout.fragment_add_card, container, false);

        questionTextInputLayout =
                (TextInputEditText) view.findViewById(R.id.text_input_question);

        answerTextInputLayout =
                (TextInputEditText) view.findViewById(R.id.text_input_answer);


        view.findViewById(R.id.button_add).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                submit();
            }
        });

        // used to display SnackBars with brief messages
        coordinatorLayout = (CoordinatorLayout) getActivity().findViewById(
                R.id.coordinatorLayout);

     //   Bundle arguments = getArguments(); // null if creating new contact

        // if editing an existing contact, create Loader to get the contact
        if (flashCardUri != null)
            getLoaderManager().initLoader(FLASHCARD_LOADER, null, this);

        return view;
    }

    private void submit() {

        String question = questionTextInputLayout.getText().toString().trim();
        String answer = answerTextInputLayout.getText().toString().trim();

        if (question.length() == 0) {
            questionTextInputLayout.setError(getString(R.string.error_input_question));
        }

        if (answer.length() == 0) {
            answerTextInputLayout.setError(getString(R.string.error_input_answer));
        }

        if (question.length() > 0 && answer.length() > 0) {
            ContentValues contentValues = new ContentValues();
            contentValues.put(DatabaseContract.TableFlashcards.COL_QUESTION, question);
            contentValues.put(DatabaseContract.TableFlashcards.COL_ANSWER, answer);

            Uri uri = getActivity().getContentResolver().insert(DatabaseContract.TableFlashcards.CONTENT_URI, contentValues);

            if (uri == null) {
                Snackbar.make(coordinatorLayout,
                        R.string.card_added, Snackbar.LENGTH_LONG).show();
            }
        } else {
            Snackbar.make(coordinatorLayout,
                    R.string.error_adding_card, Snackbar.LENGTH_LONG).show();
        }
    }

    // called by LoaderManager to create a Loader
    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        // create an appropriate CursorLoader based on the id argument;
        // only one Loader in this fragment, so the switch is unnecessary
        switch (id) {
            case FLASHCARD_LOADER:
                return new CursorLoader(getActivity(),
                        flashCardUri, // Uri of contact to display
                        null, // null projection returns all columns
                        null, // null selection returns all rows
                        null, // no selection arguments
                        null); // sort order
            default:
                return null;
        }
    }

    // called by LoaderManager when loading completes
    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
        // if the contact exists in the database, display its data
        if (data != null && data.moveToFirst()) {
            // get the column index for each data item
            int questionIndex = data.getColumnIndex(DatabaseContract.TableFlashcards.COL_QUESTION);
            int answerIndex = data.getColumnIndex(DatabaseContract.TableFlashcards.COL_ANSWER);

//            // fill EditTexts with the retrieved data
//            questionTextInputLayout.getText().setText
//
//            questionTextInputLayout.getEditableText().(
//                    data.getString(questionIndex));
//            answerTextInputLayout.getEditableText().setText(
//                    data.getString(answerIndex));

        }
    }

    // called by LoaderManager when the Loader is being reset
    @Override
    public void onLoaderReset(Loader<Cursor> loader) { }
}
