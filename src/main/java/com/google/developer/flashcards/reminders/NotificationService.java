package com.google.developer.flashcards.reminders;

import android.app.IntentService;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Intent;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.TaskStackBuilder;

import com.google.developer.flashcards.MainActivity;
import com.google.developer.flashcards.R;
import com.google.developer.flashcards.SettingsActivity;

public class NotificationService extends IntentService {
    public static int NOTIFICATION_ID = 8;
    private static final String TAG = NotificationService.class.getSimpleName();
    private NotificationManager mNotificationManager;

    public NotificationService() {
        super(TAG);
    }

    @Override
    public void onCreate() {
        super.onCreate();

        NotificationCompat.Builder builder = new NotificationCompat.Builder(this);

        builder.setContentTitle("Flashcard Practice Alert!")
                .setSmallIcon(R.drawable.small_icon)
                .setAutoCancel(true)
                .setTicker("let's get started!");


        Intent intent = new Intent(this, MainActivity.class);

        TaskStackBuilder stackBuilder = TaskStackBuilder.create(this);
        stackBuilder.addParentStack(MainActivity.class).addNextIntent(intent);
        PendingIntent pendingIntent = stackBuilder.getPendingIntent(0, PendingIntent.FLAG_UPDATE_CURRENT);

        builder.setContentIntent(pendingIntent);

        mNotificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
        mNotificationManager.notify(NOTIFICATION_ID, builder.build());

        NOTIFICATION_ID ++;

    }

    @Override
    protected void onHandleIntent(Intent intent) {

    }
}
