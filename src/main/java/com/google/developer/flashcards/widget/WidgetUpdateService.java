package com.google.developer.flashcards.widget;

import android.app.IntentService;
import android.app.PendingIntent;
import android.appwidget.AppWidgetManager;
import android.content.ComponentName;
import android.content.Intent;
import android.database.Cursor;
import android.util.Log;
import android.widget.RemoteViews;

import com.google.developer.flashcards.MainActivity;
import com.google.developer.flashcards.R;
import com.google.developer.flashcards.data.DatabaseContract;
import com.google.developer.flashcards.data.Flashcard;

import java.util.Random;
import com.google.developer.flashcards.data.DatabaseContract.TableFlashcards;

public class WidgetUpdateService extends IntentService {
    private static final String TAG = WidgetUpdateService.class.getSimpleName();

    public static final String EXTRA_WIDGET_IDS = "widget_ids";

    public WidgetUpdateService() {
        super(TAG);
    }

    @Override
    protected void onHandleIntent(Intent intent) {

        AppWidgetManager appWidgetManager = AppWidgetManager.getInstance(this);
        int[] appWidgetIds = appWidgetManager.getAppWidgetIds(new ComponentName(this,
                FlashcardWidget.class));

        for (int id : appWidgetIds) {

            int totalCards = 0;

            Cursor cursor = getContentResolver().query(TableFlashcards.CONTENT_URI,
                    null,
                    null,
                    null,
                    null);

            if (cursor != null) {
                totalCards = cursor.getCount();
            }

            if (cursor == null) {
                Log.w(TAG, "Unable to read card database");
                return;
            }

            Random randomNumberGenerator = new Random();
            int randomNumber = randomNumberGenerator.nextInt(totalCards);

            cursor.moveToPosition(randomNumber);
            Flashcard randomFlashcard = new Flashcard(cursor);

            //Load question info into widget view
            RemoteViews view = new RemoteViews(getPackageName(), R.layout.flashcard_widget);
            view.setTextViewText(R.id.appwidget_text, randomFlashcard.question.toString());

            // Create an Intent to launch MainActivity and attach an on-click listener
            // to the widget view
            intent = new Intent(getBaseContext(), MainActivity.class);
            PendingIntent pendingIntent = PendingIntent.getActivity(getBaseContext(), 0, intent, 0);
            view.setOnClickPendingIntent(R.id.widget, pendingIntent);

            //Update the widget
            appWidgetManager.updateAppWidget(id, view);


        }
    }
}

