package com.google.developer.flashcards;

import android.Manifest;
import android.app.ActionBar;
import android.app.ProgressDialog;
import android.content.ContentValues;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteQueryBuilder;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;

import com.google.developer.flashcards.data.CardRecyclerAdapter;
import com.google.developer.flashcards.CardsFragment.CardFragmentListener;
import com.google.developer.flashcards.data.CardsDBHelper;
import com.google.developer.flashcards.data.DatabaseContract;
import com.google.developer.flashcards.data.Flashcard;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

public class MainActivity extends AppCompatActivity implements CardsFragment.CardFragmentListener {


    // key for storing a contact's Uri in a Bundle passed to a fragment
    public static final String FLASHCARD_URI = "flashcard_uri";
    private static final int WRITE_REQUEST_CODE = 0;

    private CardRecyclerAdapter cardRecyclerAdapter; // adapter for recyclerView

    private CardFragmentListener listener;

    private CardsFragment cardsFragment; // displays contact list

    private Resources mResources;

    private ProgressDialog progress;

    private static boolean insertTaskRun = true;
    private CardsDBHelper mCardsDBHelper;
    private SQLiteDatabase db;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // if layout contains fragmentContainer, the phone layout is in use;
        // create and display a CardsFragment
        if (savedInstanceState == null &&
                findViewById(R.id.fragmentContainer) != null) {
            // create ContactsFragment
            cardsFragment = new CardsFragment();

            // add the fragment to the FrameLayout
            FragmentTransaction transaction =
                    getSupportFragmentManager().beginTransaction();
            transaction.add(R.id.fragmentContainer, cardsFragment);
            transaction.commit(); // display CardsFragment
        }

        mResources = getBaseContext().getResources();

        //You should not initialize your helper object using with new DatabaseHelper(context)!
        // Instead, always use DatabaseHelper.getInstance(context)

        //mCardsDBHelper = new CardsDBHelper(getBaseContext());

        mCardsDBHelper = CardsDBHelper.getInstance(getBaseContext());


        try {
            mCardsDBHelper.createDataBase();
        } catch (IOException e) {
            e.printStackTrace();
        }




    }


    public boolean onCreateOptionsMenu(Menu menu){

        getMenuInflater().inflate(R.menu.menu, menu);

        return true;
    }


    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {

            case R.id.action_settings:
                startActivity(new Intent(this, SettingsActivity.class));
                return true;
            default:
                return false;

        }
    }







//
    // display fragment for adding a new or editing an existing contact
    private void displayAddCardFragment(int viewID, Uri flashCardUri) {

        AddCardFragment addEditFragment = new AddCardFragment();

        // if editing existing contact, provide contactUri as an argument
        if (flashCardUri != null) {
            Bundle arguments = new Bundle();
            arguments.putParcelable(FLASHCARD_URI, flashCardUri);
            addEditFragment.setArguments(arguments);
        }

        // use a FragmentTransaction to display the AddEditFragment
        FragmentTransaction transaction =
                getSupportFragmentManager().beginTransaction();
        transaction.replace(viewID, addEditFragment);
        transaction.addToBackStack(null);
        transaction.commit(); // causes AddEditFragment to display
    }

    @Override
    public void onAddCard() {

        if (findViewById(R.id.fragmentContainer) != null) // phone
            displayAddCardFragment(R.id.fragmentContainer, null);
    }

    @Override
    public void onCardSelected(Uri contactUri) {

        if (findViewById(R.id.fragmentContainer) != null) // phone
            displayCardDetail(R.id.fragmentContainer, contactUri);

    }



    private void displayCardDetail(int viewID, Uri flashCardUri) {

        CardDetailFragment detailFragment = new CardDetailFragment();

        // specify contact's Uri as an argument to the DetailFragment
        Bundle arguments = new Bundle();
        arguments.putParcelable(FLASHCARD_URI, flashCardUri);
        detailFragment.setArguments(arguments);

        // use a FragmentTransaction to display the DetailFragment
        FragmentTransaction transaction =
                getSupportFragmentManager().beginTransaction();
        transaction.replace(viewID, detailFragment);
        transaction.addToBackStack(null);
        transaction.commit(); // causes DetailFragment to display

    }


//    /* FAB Click Events */
//    @Override
//    public void onClick(View v) {
//        Intent addCard = new Intent(this, AddCardActivity.class);
//        MainActivity.this.startActivity(addCard);
//    }
}
