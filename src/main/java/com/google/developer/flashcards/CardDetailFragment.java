package com.google.developer.flashcards;

/**
 * Created by srini on 8/20/16.
 */
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import com.google.developer.flashcards.data.DatabaseContract.TableFlashcards;



public class CardDetailFragment extends Fragment
        implements LoaderManager.LoaderCallbacks<Cursor> {


    private static final int FLASHCARD_LOADER = 0; // identifies the Loader


    private Uri flashCardUri; // Uri of selected flashcard

    private TextView inputQuestionTextView; // displays question
    private TextView equalsView;
    private TextView answerTextView; // displays answer

    // called when DetailFragmentListener's view needs to be created
    @Override
    public View onCreateView(
            LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        setHasOptionsMenu(true); // this fragment has menu items to display

        // get Bundle of arguments then extract the contact's Uri
        Bundle arguments = getArguments();

        if (arguments != null)
            flashCardUri = arguments.getParcelable(MainActivity.FLASHCARD_URI);

        // inflate DetailFragment's layout
        View view =
                inflater.inflate(R.layout.card_fragment_detail, container, false);

        // get the EditTexts
        inputQuestionTextView = (TextView) view.findViewById(R.id.questionTextView);
        equalsView = (TextView) view.findViewById(R.id.equalsLabelTextView);
        answerTextView = (TextView) view.findViewById(R.id.answerTextView);

        // load the contact
        getLoaderManager().initLoader(FLASHCARD_LOADER, null, this);
        return view;
    }

    // called by LoaderManager to create a Loader
    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        // create an appropriate CursorLoader based on the id argument;
        // only one Loader in this fragment, so the switch is unnecessary
        CursorLoader cursorLoader;

        switch (id) {
            case FLASHCARD_LOADER:
                cursorLoader = new CursorLoader(getActivity(),
                        flashCardUri, // Uri of contact to display
                        null, // null projection returns all columns
                        null, // null selection returns all rows
                        null, // no selection arguments
                        null); // sort order
                break;
            default:
                cursorLoader = null;
                break;
        }

        return cursorLoader;
    }

    // called by LoaderManager when loading completes
    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
        // if the contact exists in the database, display its data
        if (data != null && data.moveToFirst()) {
            // get the column index for each data item
            int questionIndex = data.getColumnIndex(TableFlashcards.COL_QUESTION);
            int answerIndex = data.getColumnIndex(TableFlashcards.COL_ANSWER);

            // fill TextViews with the retrieved data
            inputQuestionTextView.setText(data.getString(questionIndex));
            answerTextView.setText(data.getString(answerIndex));

        }
    }

    // called by LoaderManager when the Loader is being reset
    @Override
    public void onLoaderReset(Loader<Cursor> loader) { }
}