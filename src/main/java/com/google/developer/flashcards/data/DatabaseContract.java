package com.google.developer.flashcards.data;

import android.content.ContentUris;
import android.net.Uri;
import android.provider.BaseColumns;

/**
 * This class contains the public contract for the database and
 * content provider. No changes should be made to the code here.
 */
public class DatabaseContract {

    // Unique authority string for the content provider
    public static final String CONTENT_AUTHORITY = "com.google.developer.flashcards";
    // Default sort for query results
    public static final String DEFAULT_SORT_FLASHCARDS = TableFlashcards.COL_ID;

    public static final class TableFlashcards implements BaseColumns {

        // Database schema information
        public static final String TABLE_FLASHCARDS = "flashcards";

        public static final String COL_ID = "_id";
        public static final String COL_QUESTION = "question";
        public static final String COL_ANSWER = "answer";

        // base URI used to interact with the ContentProvider
        private static final Uri BASE_CONTENT_URI =
                Uri.parse("content://" + CONTENT_AUTHORITY);

        // Uri for the contacts table
        public static final Uri CONTENT_URI =
                BASE_CONTENT_URI.buildUpon().appendPath(TABLE_FLASHCARDS).build();

        // creates a Uri for a specific contact
        public static Uri buildContactUri(long id) {
            return ContentUris.withAppendedId(CONTENT_URI, id);
        }

    }






//    // Base content Uri for accessing the provider
//    public static final Uri CONTENT_URI = new Uri.Builder().scheme("content")
//            .authority(CONTENT_AUTHORITY)
//            .appendPath(TABLE_FLASHCARDS)
//            .build();





}
