package com.google.developer.flashcards.data;

import android.Manifest;
import android.content.ContentValues;
import android.content.Context;
import android.content.res.Resources;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;
import android.database.sqlite.SQLiteQueryBuilder;
import android.os.AsyncTask;

import com.google.developer.flashcards.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import com.google.developer.flashcards.data.DatabaseContract.TableFlashcards;

import static android.support.v4.app.ActivityCompat.requestPermissions;


public class CardsDBHelper extends SQLiteOpenHelper {
    private static final String TAG = CardsDBHelper.class.getSimpleName();

    public static final String DB_NAME = "flashcards.db";
    public static final File DB = new File("flashcards.db");

    public static final String PACKAGE_NAME = "com.google.developer.flashcards";

    public static final String DB_PATH = "/data/data/"+PACKAGE_NAME+"/databases/"+DB_NAME;

    public static final int DB_VERSION = 1;

    private static int count = 0;

    private static CardsDBHelper mCardsDBHelper;


    private static final String SQL_CREATE_TABLE_FLASHCARDS = "CREATE TABLE " +
            TableFlashcards.TABLE_FLASHCARDS + " (" +
            TableFlashcards._ID + " INTEGER PRIMARY KEY AUTOINCREMENT," +
            TableFlashcards.COL_QUESTION + " TEXT NOT NULL," +
            TableFlashcards.COL_ANSWER + " TEXT NOT NULL )";

    private static Resources mResources;
    private SQLiteDatabase database;


    public static synchronized CardsDBHelper getInstance(Context context) {

        // Use the application context, which will ensure that you
        // don't accidentally leak an Activity's context.
        // See this article for more information: http://bit.ly/6LRzfx
        if (mCardsDBHelper == null) {
            mCardsDBHelper = new CardsDBHelper(context.getApplicationContext());
        }
        return mCardsDBHelper;
    }



    public CardsDBHelper(Context context) {
        super(context, DB_NAME, null, DB_VERSION);

        mResources = context.getResources();
    }




    public void createDataBase() throws IOException{

        boolean dbExist = checkDataBase();

        if(dbExist){
            //do nothing - database already exist
        }else{

            //By calling this method and empty database will be created into the default system path
            //of your application so we are gonna be able to overwrite that database with our database.
            database = this.getReadableDatabase();

//            findOneIfNotCopyDb(database);
            new SaveToDBTask(database).execute();


        }

    }

    /**
     * Check if the database already exist to avoid re-copying each time you open the application.
     * @return true if it exists, false if it doesn't
     */
    private boolean checkDataBase(){

        SQLiteDatabase checkDB = null;

        try{

            checkDB = openDataBase();

        }catch(SQLiteException e){

            //database does't exist yet.

        }

        if(checkDB != null){

            checkDB.close();

        }

        return checkDB != null ? true : false;
    }

    public SQLiteDatabase openDataBase() throws SQLException {

        database = SQLiteDatabase.openDatabase(DB_PATH, null, SQLiteDatabase.OPEN_READWRITE);
        return database;

    }

    @Override
    public synchronized void close() {

        if(database != null)
            database.close();

        super.close();

    }

    @Override
    public void onCreate(SQLiteDatabase db) {

        db.execSQL(SQL_CREATE_TABLE_FLASHCARDS);

    }

//    // Add your public helper methods to access and get content from the database.
//    // You could return cursors by doing "return myDataBase.query(....)" so it'd be easy
//    // to you to create adapters for your views.
//    public static void findOneIfNotCopyDb(SQLiteDatabase database) {
//
//
//        CursorOperation operation = new CursorOperation(database) {
//            @Override
//            public Cursor provideCursor(SQLiteDatabase database) {
//
//                Cursor cursor = database.rawQuery("Select * from flashcards order by _id desc limit 1", null);
//
//                return cursor;
//            }
//
//            @Override
//            public void provideResult(SQLiteDatabase database, Cursor cursor) {
//
//                count = cursor.getCount();
//
//                if (count == 0) {
//
////                    new SaveToDBTask(database).execute();
//
//                }
//
//            }
//        };
//
//        operation.execute();
//
//    }

    private static class SaveToDBTask extends AsyncTask<String, Void, Void> {

        private SQLiteDatabase db;
        public SaveToDBTask(SQLiteDatabase db) {

            this.db = db;

        }

        @Override
        protected Void doInBackground(String... strings) {

            try {

                readFlashcardsFromResources(db);


            } catch (Exception e) {

            }

            return null;
        }
    }


    private static void readFlashcardsFromResources(SQLiteDatabase db) throws IOException, JSONException {
        StringBuilder builder = new StringBuilder();

        InputStream in = mResources.openRawResource(R.raw.flashcards);

        BufferedReader reader = new BufferedReader(new InputStreamReader(in));

        String line;
        while ((line = reader.readLine()) != null) {
            builder.append(line);
        }

        //Parse resource into key/values
        JSONObject root = new JSONObject(builder.toString());
        JSONArray flashcards = root.getJSONArray(Flashcard.KEY_FLASHCARDS);
        //Add each element to the database
        for (int i = 0; i < flashcards.length(); i++) {
            JSONObject item = flashcards.getJSONObject(i);
            ContentValues values = new ContentValues(2);

            values.put(DatabaseContract.TableFlashcards.COL_QUESTION,
                    item.getString(Flashcard.KEY_QUESTION));

            values.put(DatabaseContract.TableFlashcards.COL_ANSWER,
                    item.getString(Flashcard.KEY_ANSWER));

            db.insert(DatabaseContract.TableFlashcards.TABLE_FLASHCARDS, null, values);
        }
    }


//    @Override
//    protected void finalize() throws Throwable {
//        this.close();
//        super.finalize();
//    }


    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + TableFlashcards.TABLE_FLASHCARDS);
        onCreate(db);
    }

}
