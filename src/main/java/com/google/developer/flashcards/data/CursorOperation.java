package com.google.developer.flashcards.data;

import android.content.res.Resources;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.google.developer.flashcards.R;

import java.io.File;

/**
 * Created by srini on 8/26/16.
 */
public abstract class CursorOperation {

    private SQLiteDatabase db;

    public CursorOperation(SQLiteDatabase db){

        this.db = db;

    }

    public void execute(){


        Cursor cursor = provideCursor(db);

        provideResult(db, cursor);

        cursor.close();

    //    db.close();


    }

    public abstract Cursor provideCursor(SQLiteDatabase database);

    public abstract void provideResult(SQLiteDatabase database, Cursor cursor);


}
