package com.google.developer.flashcards.data;

import android.content.ContentProvider;
import android.content.ContentValues;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteQueryBuilder;
import android.net.Uri;
import android.support.annotation.Nullable;

import com.google.developer.flashcards.R;
import com.google.developer.flashcards.data.DatabaseContract.TableFlashcards;

public class CardsProvider extends ContentProvider {
    private static final String TAG = CardsProvider.class.getSimpleName();

    private static final int ONE_FLASHCARD = 1;
    private static final int FLASHCARDS = 2;


    private static final UriMatcher uriMatcher = new UriMatcher(UriMatcher.NO_MATCH);
    static {

        uriMatcher.addURI(DatabaseContract.CONTENT_AUTHORITY,
                TableFlashcards.TABLE_FLASHCARDS + "/#",
                ONE_FLASHCARD);

        uriMatcher.addURI(DatabaseContract.CONTENT_AUTHORITY,
                TableFlashcards.TABLE_FLASHCARDS,
                FLASHCARDS);
    }

    private CardsDBHelper mCardsDBHelper;

    @Override
    public boolean onCreate() {

        mCardsDBHelper = new CardsDBHelper(getContext());
        return true;
    }

    @Nullable
    @Override
    public String getType(Uri uri) {
        return null;

    }

    @Nullable
    @Override
    public Cursor query(Uri uri, String[] projection, String selection, String[] selectionArgs,
                        String sortOrder) {

        // create SQLiteQueryBuilder for querying contacts table
        SQLiteQueryBuilder queryBuilder = new SQLiteQueryBuilder();
        queryBuilder.setTables(TableFlashcards.TABLE_FLASHCARDS);

        switch (uriMatcher.match(uri)) {
            case ONE_FLASHCARD: // flashcard with specified id will be selected
                queryBuilder.appendWhere(
                        TableFlashcards._ID + "=" + uri.getLastPathSegment());
                break;
            case FLASHCARDS: // all flashcards will be selected
                break;
            default:
                throw new UnsupportedOperationException(
                        getContext().getString(R.string.invalid_query_uri) + uri);
        }

        // execute the query to select one or all flashcards
        Cursor cursor = queryBuilder.query(mCardsDBHelper.getReadableDatabase(),
                projection, selection, selectionArgs, null, null, TableFlashcards.COL_ID+" DESC");

        // configure to watch for content changes
        cursor.setNotificationUri(getContext().getContentResolver(), uri);
        return cursor;



    }

    @Nullable
    @Override
    public Uri insert(Uri uri, ContentValues values) {

        Uri newContactUri = null;

        switch (uriMatcher.match(uri)) {
            case FLASHCARDS:
                // insert the new flashcard --success yields new flashcard's row id
                long rowId = mCardsDBHelper.getWritableDatabase().insert(
                        TableFlashcards.TABLE_FLASHCARDS, null, values);

                // if the contact was inserted, create an appropriate Uri;
                // otherwise, throw an exception
                if (rowId > 0) { // SQLite row IDs start at 1
                    newContactUri = TableFlashcards.buildContactUri(rowId);

                    // notify observers that the database changed
                    getContext().getContentResolver().notifyChange(uri, null);
                }
                else
                    throw new SQLException(
                            getContext().getString(R.string.insert_failed) + uri);
                break;
            default:
                throw new UnsupportedOperationException(
                        getContext().getString(R.string.invalid_insert_uri) + uri);
        }

        return newContactUri;


    }

    @Override
    public int delete(Uri uri, String selection, String[] selectionArgs) {
        throw new UnsupportedOperationException("This provider does not support deletion");
    }

    @Override
    public int update(Uri uri, ContentValues values, String selection, String[] selectionArgs) {
        throw new UnsupportedOperationException("This provider does not support updates");
    }
}
