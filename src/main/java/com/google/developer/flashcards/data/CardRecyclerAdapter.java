package com.google.developer.flashcards.data;

import android.support.v7.widget.RecyclerView;

/**
 * Base class for the RecyclerView adapter to display flashcards.
 */
import android.database.Cursor;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.SimpleCursorAdapter;
import android.widget.TextView;

import com.google.developer.flashcards.R;
import com.google.developer.flashcards.data.DatabaseContract.TableFlashcards;

public class CardRecyclerAdapter
        extends RecyclerView.Adapter<CardRecyclerAdapter.ViewHolder> {

    // interface implemented by MainActivity to respond
    // when the user touches an item in the RecyclerView
    public interface CardClickListener {
        void onCardClick(Uri flashCardUri);
    }

    // nested subclass of RecyclerView.ViewHolder used to implement
    // the view-holder pattern in the context of a RecyclerView
    public class ViewHolder extends RecyclerView.ViewHolder {


        public final TextView inputQuestion;
//        public final TextView answer;
        private long rowID;

        // configures a RecyclerView item's ViewHolder
        public ViewHolder(View itemView) {
            super(itemView);
            inputQuestion = (TextView) itemView.findViewById(R.id.title);

            // attach listener to itemView
            itemView.setOnClickListener(
                    new View.OnClickListener() {
                        // executes when the contact in this ViewHolder is clicked
                        @Override
                        public void onClick(View view) {
                            clickListener.onCardClick(TableFlashcards.buildContactUri(rowID));
                        }
                    }
            );
        }

        // set the database row ID for the contact in this ViewHolder
        public void setRowID(long rowID) {
            this.rowID = rowID;
        }
    }

    // ContactsAdapter instance variables
    private Cursor cursor = null;
    private final CardClickListener clickListener;

    // constructor
    public CardRecyclerAdapter(CardClickListener clickListener) {
        this.clickListener = clickListener;
    }

    // sets up new list item and its ViewHolder
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // inflate the android.R.layout.simple_list_item_1 layout
        View view = LayoutInflater.from(parent.getContext()).inflate(
                R.layout.row_layout, parent, false);

        return new ViewHolder(view); // return current item's ViewHolder
    }

    // sets the text of the list item to display the search tag
    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        cursor.moveToPosition(position);

        holder.setRowID(cursor.getLong(cursor.getColumnIndex(TableFlashcards._ID)));

        holder.inputQuestion.setText(cursor.getString(cursor.getColumnIndex(
                TableFlashcards.COL_QUESTION)));
//        holder.answer.setText(cursor.getString(cursor.getColumnIndex(
//                TableFlashcards.COL_ANSWER)));
    }

    // returns the number of items that adapter binds
    @Override
    public int getItemCount() {
        return (cursor != null) ? cursor.getCount() : 0;
    }

    // swap this adapter's current Cursor for a new one
    public void swapCursor(Cursor cursor) {
        this.cursor = cursor;
        notifyDataSetChanged();
    }
}
